Begin3
Language:    FR, 850, French (Standard)
Title:       GCDROM
Description: Pilote de CD/DVD-ROM IDE natif SATA
Summary:     Pilote de CD/DVD-ROM IDE natif SATA pour DOS (alias « pilote ODD DOS ») prend en charge tous les contrôleurs IDE natifs SATA, tels que Intel ICH6/ICH7/ICH8, Jmicron 363/368, Nvidia CK804 MCP55/MCP51, etc.
Keywords:    CD-ROM pilote, SATA, driver
End
