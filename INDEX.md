# GCDROM

SATA native IDE CD/DVD-ROM driver that supports all SATA Native IDE controllers, such as Intel ICH6/ICH7/ICH8, Jmicron 363/368, Nvidia CK804 MCP55/MCP51 etc.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## GCDROM.LSM

<table>
<tr><td>title</td><td>GCDROM</td></tr>
<tr><td>version</td><td>2.4b</td></tr>
<tr><td>entered&nbsp;date</td><td>2019-11-11</td></tr>
<tr><td>description</td><td>SATA native IDE CD/DVD-ROM driver</td></tr>
<tr><td>keywords</td><td>CD-ROM driver, SATA, driver</td></tr>
<tr><td>author</td><td>Jack R. Ellis, marktsai0316 -AT- gmail.com, elvingoh -AT- users.sourceforge.net</td></tr>
<tr><td>original&nbsp;site</td><td>http://sourceforge.net/projects/cdromdosdrv/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2 or later</td></tr>
<tr><td>summary</td><td>SATA native IDE CD/DVD-ROM driver that supports all SATA Native IDE controllers, such as Intel ICH6/ICH7/ICH8, Jmicron 363/368, Nvidia CK804 MCP55/MCP51 etc.</td></tr>
</table>
